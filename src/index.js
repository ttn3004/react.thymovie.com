import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { Router } from 'react-router-dom';
import { Provider } from 'react-redux';
import { createBrowserHistory } from 'history';
import App from './components/App';
import store from './store'

const customHistory = createBrowserHistory();

ReactDOM.render(
    <Router history={customHistory}>
        <Provider store={store}>
    <App />
    </Provider>
    </Router>
, document.getElementById('root'));
