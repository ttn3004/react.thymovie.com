import {
    FILM_ITEM_START,
    FILM_ITEM_SUCCESS,
    FILM_ITEM_FAILURE
}
    from '../constants/actionTypes';
import {
    FILM_ITEM_REV_START,
    FILM_ITEM_REV_SUCCESS,
    FILM_ITEM_REV_FAILURE,
    
}
    from '../constants/actionTypes';

const stateInit = {
    filmItem: {},
    isLoading: false,
    error: '',
   
};

const filmItemReducer = (state = stateInit, action) => {
    switch (action.type) {
        case FILM_ITEM_START:
            return {
                ...state,
                filmItem: {},
                isLoading: true
            };
        case FILM_ITEM_SUCCESS:
            return {
                ...state,
                isLoading: false,
                filmItem: action.data
            };
        case FILM_ITEM_FAILURE:
            return {
                ...state,
                isLoading: false,
                error: action.error,
                filmItem: {}
            };
        case FILM_ITEM_REV_START:
            return {
                ...state,
                isLoading: true,

            }
        case FILM_ITEM_REV_SUCCESS:
            return {
                ...state,
                isLoading: false,
                filmItem: action.data
            }
        case FILM_ITEM_REV_FAILURE:
            return {
                ...state,
                isLoading: false,
                filmItem: {}
            }

        default:
            return state;
    }
};
export default filmItemReducer;