import { combineReducers } from 'redux';
import filmListReducer from './filmList';
import filmSortReducer from './filmSort';
import filmItemReducer from './filmItem';
import filmKeyTitanicReducer from './filmKeyTitanic';
import appReducer from './app';
import registerReducer from './register';



const rootReducer = combineReducers ({
    filmListState: filmListReducer,
    filmSortState: filmSortReducer,
    filmItemState: filmItemReducer,
    filmKeyTitanicState : filmKeyTitanicReducer,
    appState: appReducer,
    registerState: registerReducer,
    
    
});

export default rootReducer;