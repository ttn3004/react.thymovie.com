import { FILM_KEY_TITANIC_START, 
    FILM_KEY_TITANIC_SUCCESS,
    FILM_KEY_TITANIC_FAILURE }
from '../constants/actionTypes';

const stateInit = {
    filmKeyTitanic: [],
    isLoading: false,
    error: ''
};

const filmKeyTitanicReducer = (state=stateInit, action) => {
    switch (action.type) {
        case FILM_KEY_TITANIC_START :
            return {
                ...state,
                isLoading: true
            };
        case FILM_KEY_TITANIC_SUCCESS:
            return {
                ...state,
                isLoading: false,
                filmKeyTitanic: action.data
            };
        case FILM_KEY_TITANIC_FAILURE:
            return {
                ...state,
                isLoading: false,
                error: action.error,
                filmKeyTitanic: []
            }
    
        default:
            return state;
    }
};
export default filmKeyTitanicReducer;