import { FILM_SORT_START,
    FILM_SORT_SUCCESS,
    FILM_SORT_FAILURE } 
from '../constants/actionTypes';

const stateInit = {
    filmSort: [],
    isLoading: false,
    error: ''
};

const filmSortReducer = (state=stateInit, action) => {
    switch (action.type) {
        case FILM_SORT_START:
            return {
                ...state,
                isLoading: true
            };
        case FILM_SORT_SUCCESS:
            return {
                ...state,
                isLoading: false,
                filmSort: action.data
            };
        case FILM_SORT_FAILURE:
            return {
                ...state,
                isLoading: false,
                error: action.error,
                filmSort: []
            }
        default:
            return state;

    }
};

export default filmSortReducer;