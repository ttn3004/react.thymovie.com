import {APP_REGISTER_START,
    APP_REGISTER_SUCCESS,
    APP_REGISTER_FAILURE,
    APP_AUTH_SUCCESS} from '../constants/actionTypes';

const stateInit = {
    userRegister: {},
    tokenApp: '',
    isLoading: false,
    error: ''
};

const registerReducer = (state= stateInit, action) => {
    switch (action.type) {
        case APP_REGISTER_START :
            return {
                ...state,
                isLoading:true,
                error :''
            }
        case APP_REGISTER_SUCCESS :
            return {
                ...state,
                isLoading: false,
                tokenApp: action.data
            }
        case APP_REGISTER_FAILURE :
            return {
                ...state,
                isLoading: false,
                error: action.error,
                userRegister:{},
                tokenApp: ''
            }
        case APP_AUTH_SUCCESS : 
            return {
                ...state,
                isLoading: false,
                userRegister: action.data ,
                error: ''
            }
    
        default:
            return state;
    }
};

export default registerReducer;