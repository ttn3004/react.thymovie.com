import { FILM_LIST_START,FILM_LIST_SUCCESS,FILM_PAGING_SUCCESS,FILM_LIST_FAILURE } from '../constants/actionTypes';

const initialState = {
    filmList: [],
    isLoading: false,
    error:'',
    postDataTableObject:{
      page: 0,
      totalPage:0,
      pageSize:10,
      totalSize:0,
      filmType:'all',
      sorted: [],
      filtered: [],
      menuFiltered:[],
      listFiltered:[],
      searchByKeyword:''
    }
  };

  const filmListReducer = (state = initialState, action) => {
    switch (action.type) {
      case FILM_LIST_START:
        return {
          ...state,
          isLoading: true,
          postDataTableObject:{
            ...state.postDataTableObject,
            listFiltered:[]
          }
        };
      case FILM_LIST_SUCCESS:
        return {
          ...state,
          isLoading: false,
          filmList: action.data
        };
      case FILM_PAGING_SUCCESS:
        return {
          ...state,
          isLoading: false,
          postDataTableObject: action.data
        };
      case FILM_LIST_FAILURE:
        return {
          ...state,
          isLoading: false,
          error: action.error,
          filmList:[],
          postDataTableObject:{}
        };
      default:
        return state;
    }
  };
  export default filmListReducer;
  
  

