import {
    APP_LOGIN_START,
    APP_LOGIN_SUCCESS,
    APP_LOGIN_FAILURE,
    APP_AUTH_SUCCESS,
    APP_BREADCRUMBS_SUCCESS,
    APP_SAVE_WISHLIST_START,
    APP_SAVE_WISHLIST_SUCCESS,
    APP_SAVE_WISHLIST_FAILURE,
    DELETE_WISHLIST_START,
    DELETE_WISHLIST_SUCCESS,
    DELETE_WISHLIST_FAILURE,
    DELETE_TO_MY_LIST_START,
    DELETE_TO_MY_LIST_SUCCESS,
    DELETE_TO_MY_LIST_FAILURE,
    ADD_TO_MY_LIST_START,
    ADD_TO_MY_LIST_SUCCESS,
    ADD_TO_MY_LIST_FAILURE
}
    from '../constants/actionTypes';
import {
    APP_LOGOUT_START,
    APP_LOGOUT_SUCCESS,
    APP_LOGOUT_FAILURE
} from '../constants/actionTypes';

const stateInit = {
    wistlist_lines: {},
    wishlist: {},
    userApp: {},
    links: [{ name: 'Home', url: '/' }],
    tokenApp: '',
    isLoading: false,
    error: ''
};

const appReducer = (state = stateInit, action) => {

    switch (action.type) {
        case APP_LOGIN_START:
            return {
                ...state,
                isLoading: true,
                error: ''
            }
        case APP_LOGIN_SUCCESS:
            return {
                ...state,
                isLoading: false,
                tokenApp: action.data
            }
        case APP_LOGIN_FAILURE:
            return {
                ...state,
                isLoading: false,
                error: action.error,
                userApp: {},
                tokenApp: ''
            }
        case APP_AUTH_SUCCESS:
            return {
                ...state,
                isLoading: false,
                userApp: action.data
            }
        case APP_LOGOUT_START:
            return {
                ...state,
                isLoading: true,
            }
        case APP_LOGOUT_SUCCESS:
            return {
                ...state,
                isLoading: false,
                tokenApp: '',
                userApp: {}
            }
        case APP_LOGOUT_FAILURE:
            return {
                ...state,
                isLoading: false,
                error: action.error,
                userApp: {},
                tokenApp: ''
            }
        case APP_BREADCRUMBS_SUCCESS:
            return {
                ...state,
                links: action.data
            }
        case APP_SAVE_WISHLIST_START:
            return {
                ...state,
                isLoading: true,
            }
        case APP_SAVE_WISHLIST_SUCCESS:
            return {
                ...state,
                isLoading: false,
                wishlist: action.data
            }
        case APP_SAVE_WISHLIST_FAILURE:
            return {
                ...state,
                isLoading: false,
                userApp: {}
            }
        case DELETE_WISHLIST_START:
            return {
                ...state,
                isLoading: false,

            }
        case DELETE_WISHLIST_SUCCESS:
            return {
                ...state,
                isLoading: false,
                wishlist: action.data
            }
        case DELETE_WISHLIST_FAILURE:
            return {
                ...state,
                isLoading: false,
                userApp: {}
            }

        case ADD_TO_MY_LIST_START:
            return {
                ...state,
                isLoading: true,

            }
        case ADD_TO_MY_LIST_SUCCESS:
            return {
                ...state,
                isLoading: false,
                wistlist_lines: action.data

            }
        case ADD_TO_MY_LIST_FAILURE:
            return {
                ...state,
                isLoading: false,
                error: action.error,
                userApp: {}
            }

        case DELETE_TO_MY_LIST_START:
            return {
                ...state,
                isLoading: false,

            }
        case DELETE_TO_MY_LIST_SUCCESS:
            return {
                ...state,
                isLoading: false,
                wistlist_lines: action.data
            }
        case DELETE_TO_MY_LIST_FAILURE:
            return {
                ...state,
                isLoading: false,
                userApp: {},
                error: action.error
            }



        default:
            return state;
    }
};

export default appReducer;


