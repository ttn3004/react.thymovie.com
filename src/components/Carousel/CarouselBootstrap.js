import React from 'react';
import { Carousel, Card, Row, Col } from 'react-bootstrap';

import { Link } from 'react-router-dom';
import { useState } from 'react';

const CarouselBootstrap = props => {
  const [index, setIndex] = useState(0);
  const [direction, setDirection] = useState(null);

  const handleSelect = (selectedIndex, e) => {
    setIndex(selectedIndex);
    setDirection(e.direction);
  };

  const calculateGridPos = nb => {
    const count = nb || 1;
    const nbColByRow = Math.floor(12 / count);
    return parseInt(nbColByRow);
  };

  return (
    <React.Fragment>
      <Carousel
        className="my-3 "
        controls={false}
        touch={true}
        indicators={true}
        activeIndex={index}
        direction={direction}
        onSelect={handleSelect}
      >
        {props.filmList.map((arr, idx) => {
          return (
            <Carousel.Item key={idx}>
              <Row noGutters>
                {arr.map((item, id) => {
                  let urlFilm = 'films';
                  if (item.film_type_id === 12) urlFilm = 'series';
                  return (
                    <Col
                      // md={2}
                      //className="p-0"
                      xs={calculateGridPos(props.count)}
                      sm={calculateGridPos(props.count)}
                      md={calculateGridPos(props.count)}
                      lg={calculateGridPos(props.count)}
                      key={id}
                    >
                      <Card key={id} style={{ border: 'none' }}>
                        <Link
                          to={`/${urlFilm}/${item.id}/${item.title}`}
                          exact="true"
                        >
                          {/* <Card.Title  style={{ textAlign: 'center', color:'white' }}>{item.title}</Card.Title> */}

                          <Card.Img
                            style={{
                              width: '100%',
                              maxHeight: '30vw',
                              minHeight: '20vw',
                              objectFit: 'cover'
                            }}
                            className="img-fluid"
                            variant="top"
                            src={
                              item.film_type_id === 12
                                ? `http://thymovie.com/image/large/thymovies-serie-img/${item.idcine}/${item.images_path}`
                                : `http://thymovie.com/image/large/thymovies-img/${item.idcine}/${item.images_path}`
                            }
                          />
                        </Link>
                        {/* <Card.ImgOverlay style={{display:'flex', alignItems:'flex-end', }} > 
                                                    
                                                </Card.ImgOverlay> */}
                      </Card>
                    </Col>
                  );
                })}
              </Row>
            </Carousel.Item>
          );
        })}
      </Carousel>
    </React.Fragment>
  );
};
export default CarouselBootstrap;
