import React from 'react';
import CarouselBootstrap from './CarouselBootstrap';


const CarouselFactory = (props) => {
    const constructUnits = (arr) => {
        //neu co props thi lay props ko co thi 1 colonne
        const count = props.count || 1;
        const arrUnit = [];
        //le nb de carousel item
        const nbIteration = Math.floor(arr.length/count);
        //item con lai : lay so du : length modulo cho count (cot)
        const nbRest = arr.length % count;
        let begin = 0;
        let end = count;

        for (let i = 0; i< nbIteration; i++) {
            let unit = arr.slice(begin, end);
            arrUnit.push(unit);
            begin= end;
            end += count;
        }
        if(nbRest > 0) {
            let unit = arr.slice (begin, begin + nbRest);
            arrUnit.push(unit);
        }
        return arrUnit;
        
    };
    return <CarouselBootstrap filmList={constructUnits(props.filmList)} count={props.count} />
};
export default CarouselFactory;
