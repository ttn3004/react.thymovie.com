import React from 'react';
import { Container, Form, FormGroup,  FormControl, FormText, Col, Button, Modal, Image } from 'react-bootstrap';
import { connect } from 'react-redux';
import { doBreadcrumbs } from '../actions/app';
import { Row } from 'react-bootstrap';
import { doSaveWishlist, doDeleteWishlist, doDeleteToMyList } from '../actions/app';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faListAlt } from '@fortawesome/free-solid-svg-icons';
import { faTrashAlt } from '@fortawesome/free-regular-svg-icons';
import { Link } from 'react-router-dom';

class Protected extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            nameWishlist: '',
            show: false,
            idWishlist: '',
            itemToDelete: {}
        }
    }
    componentDidMount() {
        this.props.initBreadcrumbs([{ name: 'Home', url: '/' }, { name: 'Profil', url: '/protected', active: 'true' }]);
    }
    checkUserAuthValid = obj => {
        for (var key in obj) {
            //check xem co property trong obj json
            if (obj.hasOwnProperty(key)) {
                return true;
            }
        }
        return false;
    }
    handleClose = () => {
        this.setState({ show: false })
    }
    handleDelete = () => {
        this.props.deleteWishlist(
            this.state.itemToDelete.id
        )
        this.setState({
            show: false,
            itemToDelete: {}
        })
    }
    handleWishlist = (event) => {
        //de khong bi giut khi ajouter nv collection
        event.preventDefault()
        this.props.saveWishlist(
            this.state.nameWishlist
        );
    }
    handleNameWishlist(event) {
        this.setState({ nameWishlist: event.target.value })
    }

    render() {
        return (
            <React.Fragment>
                <Container className="my-3" fluid={true} style={{ maxHeight: '100%' }}>
                    <h3> Vous pourriez commencer par ajouter une collection</h3>
                    <hr color="#a6a6a6" />
                    <Container>
                        <Row>
                            <Col md={12} lg={8} className="my-4">
                                {
                                    this.checkUserAuthValid(this.props.userApp.wishlists) &&
                                    this.props.userApp.wishlists.map((item, idx) => {
                                        return (
                                            <React.Fragment key={idx}>
                                                <Row>
                                                    <Col className="my-3" md={11}>
                                                        <h4><FontAwesomeIcon color="#fc26aa" icon={faListAlt} /> {item.label} </h4>
                                                        <Row >
                                                            {item.wistlist_lines.map((item, idx) => {
                                                                let urlFilm = 'films';
                                                                if (item.film_type_id === 12) urlFilm = 'series';
                                                                return (

                                                                    <Col xs={6} md={3} key={idx} className="my-3">
                                                                        <Image
                                                                            style={{ width: '100%', }}
                                                                            fluid
                                                                            src={
                                                                                item.film_type_id === 12 ?
                                                                                    `http://thymovie.com/image/large/thymovies-serie-img/${item.idcine}/${item.images_path}`
                                                                                    : `http://thymovie.com/image/large/thymovies-img/${item.idcine}/${item.images_path}`
                                                                            }
                                                                        /> <br />
                                                                        {/* style={{ position: 'relative' }} style={{ position: 'absolute', bottom: '5px', right: '-3px' }} */}
                                                                        <div style={{ display: 'flex', flexDirection: 'row', alignItems: 'flex-end', justifyContent: 'space-between' }}>
                                                                            <Link style={{color:'white'}} to={`/${urlFilm}/${item.id}/${item.title}`} exact="true">
                                                                                <h5 className="mt-3">{item.title}</h5>
                                                                            </Link>
                                                                            <Button variant="link">
                                                                                <FontAwesomeIcon onClick={() => {
                                                                                    this.props.deleteToMyList(
                                                                                        item.wishlist_line_id)
                                                                                }} color="#a6a6a6" size="xs" icon={faTrashAlt} />
                                                                            </Button>
                                                                        </div>
                                                                    </Col>

                                                                )
                                                            })}
                                                        </Row>
                                                    </Col>
                                                    <Col className="my-3" md={1}>
                                                        {/* bouton onClick pour show le modal et set state de item */}
                                                        <Button onClick={() => {
                                                            this.setState({
                                                                show: true,
                                                                itemToDelete: item
                                                            })
                                                        }} size="sm" variant="light">
                                                            <FontAwesomeIcon size="sm" color="#fc26aa" icon={faTrashAlt} />
                                                        </Button>
                                                    </Col>

                                                </Row>
                                                <hr color="#a6a6a6" />

                                            </React.Fragment>
                                        )
                                    })
                                }
                            </Col>
                            <Col md={12} lg={4} >
                                <div className="my-4" style={{ padding: '25px', border: '1px solid white', marginRight: 'auto' }} >
                                    <Form>
                                        <FormGroup>
                                            <h4 className="">Nouvelle collection</h4>
                                            <hr color="#a6a6a6" />
                                            <FormText style={{ fontSize: '13px' }} className="text-white-50 mb-3 mt-4 text-center"> Créer une nouvelle liste de collection des films </FormText>
                                            <FormControl onChange={this.handleNameWishlist.bind(this)} className="text-center" type="text" placeholder="Le nom de la collection" />
                                        </FormGroup>
                                        <Button onClick={this.handleWishlist} style={{ width: '100%',backgroundColor: '#fc26aa', borderColor: '#fc26aa' }} variant="primary" type="submit">
                                            Enregistrer
                                        </Button>
                                    </Form>
                                </div>
                            </Col>

                        </Row>
                    </Container>
                </Container>
                <Modal style={{ color: '#121F40' }} show={this.state.show} onHide={this.handleClose}>
                    <Modal.Header closeButton>
                        <Modal.Title>Êtes-vous sûr de supprimer ?</Modal.Title>
                    </Modal.Header>
                    {/* this.state.itemToDelete.label co the nhan biet label nao vi khi onClick vao bouton o tren da giu duoc state cua item */}
                    <Modal.Body>La collection : <strong>{this.state.itemToDelete.label}</strong> sera supprimée.
                        L'action est irréversible. Voulez-vous continuer ?
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={this.handleClose}>
                            Annuler
                        </Button>
                        <Button variant="primary" onClick={this.handleDelete}>
                            Supprimer
                        </Button>
                    </Modal.Footer>
                </Modal>
            </React.Fragment>
        )
    }
}

const mapStatetoProps = state => {
    return {
        isLoading: state.appState.isLoading,
        userApp: state.appState.userApp,
        error: state.appState.error
    };
};

const mapDispatchToProps = dispatch => ({
    initBreadcrumbs: (arr) => dispatch(doBreadcrumbs(arr)),
    saveWishlist: (nameWishlist) => dispatch(doSaveWishlist(nameWishlist)),
    deleteWishlist: (idWishlist) => dispatch(doDeleteWishlist(idWishlist)),
    deleteToMyList: (idWl_line) => dispatch(doDeleteToMyList(idWl_line))
})
export default connect(mapStatetoProps, mapDispatchToProps)(Protected);