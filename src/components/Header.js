import React from 'react';
import {
  Navbar,
  Nav,
  Container,
  Dropdown,
  DropdownButton
} from 'react-bootstrap';
import { NavLink, Link } from 'react-router-dom';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch, faUser } from '@fortawesome/free-solid-svg-icons';
import { connect } from 'react-redux';
import { doUserLogout } from '../actions/app';
import { withRouter } from 'react-router-dom';
import { AsyncTypeahead } from 'react-bootstrap-typeahead';
import 'react-bootstrap-typeahead/css/Typeahead.css';
import { filmSearchByKwApi } from '../api/film';
const styleHeader = {
  zIndex: 1000,
  marginBottom: '22px'
  //color: '#f64c72'
  //paddingBottom: '6px',
  //marginLeft: '10px',
};

class Header extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      keyword: '',
      options: [],
      selectedFilms: []
    };
  }
  checkUserAuthValid = obj => {
    for (var key in obj) {
      //check xem co property trong obj json
      if (obj.hasOwnProperty(key)) {
        return true;
      }
    }
    return false;
  };

  handleLogout = () => {
    this.props.logoutUserApp();
    //sau khi logout thi tro ve home
    this.props.history.push('/');
  };

  //asynchronous-searching
  _handleSearch = query => {
    this.setState({ isLoading: true });
    this.makeAndHandleRequest(query);
    /*  .then(({options}) => {
        this.setState({
          isLoading: false,
          options,
        });
      }); */
  };
  makeAndHandleRequest = async query => {
    let options = await filmSearchByKwApi(query);
    this.setState({
      isLoading: false,
      options
    });
  };

  handleInputChange = (keyword, e) => {
    this.setState({
      keyword
    });
  };
  handleChange = selectedFilms => {
    this.setState({
      selectedFilms
    });
  };
  handleSubmit = event => {
    event.preventDefault();
    /**
     * Si l'utilisateur a choisi un film dans le input, => rediriger directement vers le page du film ou de série par son genre et son id
     */
    if (
      Array.isArray(this.state.selectedFilms) &&
      this.state.selectedFilms.length > 0
    ) {
      const filmSelected = this.state.selectedFilms[0];
      if (filmSelected) {
        if (filmSelected.film_type_id === 12)
          //si il est une série
          this.props.history.push(
            `/series/${filmSelected.id}/${filmSelected.title}`
          );
        else
          this.props.history.push(
            `/films/${filmSelected.id}/${filmSelected.title}`
          );
      }
    } else {
      this.props.history.push(`/search/keyword/${this.state.keyword}`);
    }
  };
  handleKeydown = event => {
    // 'keypress' event misbehaves on mobile so we track 'Enter' key via 'keydown' event
    if (event.key === 'Enter') {
      this.handleSubmit(event);
    }
  };
  render() {
    return (
      <div style={styleHeader}>
        <Navbar
          style={{ backgroundColor: '#1d2e59', minHeight: '4rem' }}
          fixed="top"
          variant="dark"
          expand="md"
        >
          <Container fluid={true} className="">
            <Navbar.Brand style={{ fontWeight: 'bold' }} as={NavLink} to="/">
              Thymovie
            </Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse
              id="basic-navbar-nav "
              className="justify-content-between"
            >
              <Nav style={{ margin: '8px', padding: '0px' }}>
                <Nav.Link as={NavLink} to="/" exact>
                  Accueil
                </Nav.Link>
                <Nav.Link as={NavLink} to="/films">
                  Films
                </Nav.Link>
                <Nav.Link as={NavLink} to="/series">
                  Séries
                </Nav.Link>
              </Nav>

              <Nav>
                <Form
                  inline
                  onSubmit={this.handleSubmit}
                  onKeyDown={this.handleKeydown}
                >
                  {/* changer width:'auto' pour width change auto selon la taille de l'écran */}

                  <AsyncTypeahead
                    id="header-searchByKw"
                    {...this.state}
                    labelKey="title"
                    minLength={3}
                    isLoading={this.state.isLoading}
                    placeholder="Nom du film, nom d'acteur..."
                    onSearch={this._handleSearch}
                    onInputChange={this.handleInputChange}
                    onChange={this.handleChange}
                    renderMenuItemChildren={option => (
                      <div key={option.id}>
                        <p>{option.title}</p>
                      </div>
                    )}
                  />
                  <Button variant="link" onClick={this.handleSubmit}>
                    <FontAwesomeIcon
                      style={{ color: '#fc26aa' }}
                      icon={faSearch}
                      //size="md"
                    />
                  </Button>
                </Form>

                {this.checkUserAuthValid(this.props.userApp) ? (
                  //  muon changer color du dropdownbouton su dung variant défini dans css
                  <DropdownButton
                    style={{ display: 'inline' }}
                    id="dropdown-menu-align-right"
                    alignRight
                    title={this.props.userApp.name}
                  >
                    {this.props.userApp && (
                      <Dropdown.Item tag="a" href="/protected">
                        Vos collections
                      </Dropdown.Item>
                    )}

                    <Dropdown.Item onClick={this.handleLogout}>
                      Déconnexion
                    </Dropdown.Item>
                  </DropdownButton>
                ) : (
                  <Nav.Link
                    as={NavLink}
                    style={{ borderBottomStyle: 'none' }}
                    to="/login"
                    exact
                  >
                    <FontAwesomeIcon
                      style={{ color: '#fc26aa' }}
                      icon={faUser}
                      size="lg"
                    />
                  </Nav.Link>
                )}
              </Nav>
            </Navbar.Collapse>
          </Container>
        </Navbar>
      </div>
    );
  }
}

const mapStatetoProps = state => {
  return {
    isLoading: state.appState.isLoading,
    userApp: state.appState.userApp,
    error: state.appState.error
  };
};
const mapDispatchToProps = dispatch => ({
  logoutUserApp: () => dispatch(doUserLogout())
});

export default withRouter(connect(mapStatetoProps, mapDispatchToProps)(Header));
