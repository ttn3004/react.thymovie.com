import React from 'react';
import DataTablePagingComponent from './../Film/DataTablePagingComponent';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { doBreadcrumbs } from '../../actions/app';

class SerieListPaging extends React.Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    this.props.initBreadcrumbs([
      { name: 'Home', url: '/' },
      { name: 'Séries', url: '/series', active: 'true' }
    ]);
  }
  render() {
    return (
      <div>
        <React.Fragment>
          <DataTablePagingComponent type="serie" />
        </React.Fragment>
      </div>
    );
  }
}
const mapDispatchToProps = dispatch => ({
  initBreadcrumbs: arr => dispatch(doBreadcrumbs(arr))
});
export default withRouter(connect(null, mapDispatchToProps)(SerieListPaging));
