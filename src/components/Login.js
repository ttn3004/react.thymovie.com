import React from 'react';
import { Button, Container, Alert } from 'react-bootstrap';
import { doUserLogin } from '../actions/app';
import { doUserRegister } from '../actions/register'
import { connect } from 'react-redux';
import { getUserAppSelector } from '../selectors/app';
import {doBreadcrumbs} from '../actions/app';
import {withRouter} from 'react-router-dom';

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
        };
    }
    //componentDidMount la de greffer composant vao DOM de récupérer données JSON
    componentDidMount () {
        this.props.initBreadcrumbs([{name:'Home',url:'/' },{name:'Login', url:'/login',active:'true' }])
    }
    //handleLogin de lancer action de nhan thong tin cua user
    handleLogin = () => {
        this.props.loginUserApp(
            this.state.email,
            this.state.password);
    };
    handleRegister = () => {
        this.props.registerUserApp(
            this.state.email,
            this.state.password);
    };
    handleChangeEmail(event) {
        this.setState({ email: event.target.value });
    }
    handleChangePassword(event) {
        this.setState({ password: event.target.value });
    }
    render() {
        return (
            // 
            <Container style={{ maxHeight:'100%'}}>

                <div className="row">
                    <div className="col-md-4" >
                    </div>
                    <div className="col-md-4" style={{ width: '500px', height: '100px' }}  >

                    </div>
                    <div className="col-md-4" >
                    </div>
                </div>

                <div className="row">
                    <div className="col-md-4 " >
                    </div>
                    <div className="col-md-4" style={{
                        width: '500px', height: '300px'
                    }}>
                        {/* <div class="col-md-6 align-items-center"> */}

                        <h2 className="text-center my-3"> Accès membre </h2>

                        <input style={{
                            width: '100%',
                            display: 'block',
                            marginLeft: 'auto',
                            marginRight: 'auto'
                        }} className="mb-2"
                        required title="3 charactères minimum"
                            pattern=".{3,100}"
                            type="text" placeholder="Enter Username" name="uname" onChange={this.handleChangeEmail.bind(this)} />
                        <input style={{
                            width: '100%',
                            display: 'block',
                            marginLeft: 'auto',
                            marginRight: 'auto'
                        }}
                        required title="3 charactères minimum"
                            pattern=".{3,100}"
                            type="password" placeholder="Enter Password" name="pwd" onChange={this.handleChangePassword.bind(this)} />
                        <br />
                        <Button style={{
                            width: '100%',
                            display: 'block',
                            marginLeft: 'auto',
                            marginRight: 'auto',
                            backgroundColor: '#fc26aa',
                            borderColor: '#fc26aa'
                        }}
                            onClick={this.handleLogin}>
                            Connectez-vous
                        </Button>
                        <br />
                        <Button style={{
                            width: '100%',
                            display: 'block',
                            marginLeft: 'auto',
                            marginRight: 'auto',
                            backgroundColor: '#1d2e59',
                            borderColor: '#1d2e59'
                        }}
                            onClick={this.handleRegister}>
                            Inscrivez-vous
                        </Button>
                        <br/>
                        {this.props.error || this.props.errorReg  ? 
                        <Alert  variant="danger">
                            <span> {this.props.error} </span>|| <span> {this.props.errorReg} </span>
                        </Alert> : '' }

                       
                        
                    </div>

                    <div className="col-md-4">
                    </div>
                </div>



                <div className="row">
                    <div className="col-md-4" >
                    </div>
                    <div className="col-md-4" style={{ width: '500px', height: '200px' }}  >

                    </div>
                    <div className="col-md-4" >
                    </div>
                </div>

            </Container>
        )
    }

}

const mapStatetoProps = state => {

    return {

        isLoading: state.appState.isLoading,
        userApp: getUserAppSelector(state.appState),
        error: state.appState.error,
        //   isLoadingReg: state.registerState.isLoading ,
        //   userRegister: state.registerState.userRegister,
        errorReg: state.registerState.error
    };
};
const mapDispatchToProps = dispatch => ({
    loginUserApp: (email, pass) => dispatch(doUserLogin(email, pass)),
    registerUserApp: (email, pass) => dispatch(doUserRegister(email, pass)),
    initBreadcrumbs:(arr) => dispatch(doBreadcrumbs(arr))
});

export default withRouter(connect(
    mapStatetoProps,
    mapDispatchToProps
)(Login));



