import React from 'react';

import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { doBreadcrumbs } from '../../actions/app';
import SearchPagingComponent from '../Film/SearchPagingComponent';

class SearchListPaging extends React.Component {
  constructor(props) {
    super(props);
    const keyword = this.props.match.params.keyword;
    const filterid = this.props.match.params.filterid;
    const filtername = this.props.match.params.filtername;
    const filtervalue = this.props.match.params.filtervalue;
    if (typeof keyword !== 'undefined' && keyword !== '') {
      this.state = {
        keyword: keyword,
        type: 'kw'
      };
      this.switchBread(this.state.type, keyword);
    } else if (typeof filtername !== 'undefined' && filtername !== '') {
      this.state = {
        filtername: filtername,
        filterid:filterid,
        filtervalue: filtervalue,
        type: 'filter'
      };
      this.switchBread(this.state.type, filtervalue);
    }
  }
  componentDidMount() {
}

  switchBread = (type, label) => {
    if (type === 'kw') {
      this.props.initBreadcrumbs(this.getBreadcrumbs(type, label));
    } else if (type === 'filter') {
      this.props.initBreadcrumbs(this.getBreadcrumbs(type, label));
    }
  };
  getBreadcrumbs = (type, label) => {
    const breadname =
      'Recherche ' +
      (type === 'kw' ? ' avec le mot clé "' : ' les films dans ') +
      label +
      '"';
    return [
      { name: 'Home', url: '/' },
      {
        name: breadname,
        url: '/search',
        active: 'true'
      }
    ];
  };

  componentDidUpdate(prevProps) {
    if (this.props.match.path.includes('/keyword/')) {
      const oldKw = prevProps.match.params.keyword;
      const newKw = this.props.match.params.keyword;
      if (oldKw !== newKw) {
        this.setState({});
        this.setState({
          keyword: newKw,
          type: 'kw'
        });
        this.switchBread(this.state.type, newKw);
      }
    } else if (this.props.match.path.includes('/filter/')) {
      const oldFilterid = prevProps.match.params.filterid;
      const newFilterid = this.props.match.params.filterid;
      const newFiltername = this.props.match.params.filtername;
      const newFiltervalue = this.props.match.params.filtervalue;
      if (oldFilterid !== newFilterid) {
        this.setState({});
        this.setState({
          filtername: newFiltername,
          filtervalue: newFiltervalue,
          filterid:newFilterid,
          type: 'filter'
        });
        this.switchBread(this.state.type, newFiltervalue);
      }
    }
  }

  render() {
    return (
      <div>
        <React.Fragment>
          <SearchPagingComponent
            type="all"
            findingsearch={this.state.type}
            filterid={this.state.filterid}
            filtername={this.state.filtername}
            filtervalue={this.state.filtervalue}
            searchByKeyword={this.state.keyword}
          />
        </React.Fragment>
      </div>
    );
  }
}
const mapDispatchToProps = dispatch => ({
  initBreadcrumbs: arr => dispatch(doBreadcrumbs(arr))
});
export default withRouter(connect(null, mapDispatchToProps)(SearchListPaging));
