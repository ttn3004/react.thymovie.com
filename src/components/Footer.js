import React from 'react';
import {
  Navbar,
  Container,
  Row,
  Col
} from 'react-bootstrap';
import { NavLink } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFacebookSquare, faTwitterSquare, faInstagram, faYoutubeSquare, faVimeoSquare, faGooglePlusSquare } from '@fortawesome/free-brands-svg-icons';


class Footer extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <React.Fragment>
        <Navbar style={{ backgroundColor: '#1d2e59', fontSize:'12px', marginTop:'100px' }}
          variant="dark"
          expand="md"
          className="py-5">
          <Container fluid={true}>
            <Row style={{
              flex: 1,
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}>

              <Col xs={12} sm={4} xl={4} >
                <Navbar.Brand as={NavLink} to="/">
                  Thymovie
                </Navbar.Brand>
                <p className="text-justify"> Le site thymovie.com est développé par Thy Ngoc PHAM NGUYEN.
                  Il permet aux spectateurs de partager leurs avis et leurs notes
                  sur les films et les mettre dans les collections préférées. </p>
              </Col>

              <Col xs={12} sm={4} xl={4}  className="justify-content-center text-center">
                <h6>Suivez nous sur les réseaux sociaux</h6>
                <div style={{fontSize:'2rem', color: '#fc26aa'}}>
                <FontAwesomeIcon className="mr-2" icon={faFacebookSquare}/>
                <FontAwesomeIcon className="mr-2" icon={faTwitterSquare} />
                <FontAwesomeIcon className="mr-2" icon={faInstagram} />
                <FontAwesomeIcon className="mr-2" icon={faYoutubeSquare} />
                <FontAwesomeIcon className="mr-2" icon={faVimeoSquare} />
                <FontAwesomeIcon className="mr-2" icon={faGooglePlusSquare}/>
                </div>
              </Col>

              <Col xs={12} sm={4} xl={4} >
                <h6>Notre contact </h6>
                101 E 129th St, East Chicago,
                IN 46312, US <br/>
                Phone: 001-1234-88888 <br/>
                Email: contact@thymovie.com
              </Col>

            </Row>
          </Container>
        </Navbar>
      </React.Fragment>
    );
  }
}



export default Footer;
