import React from 'react';
import { connect } from 'react-redux';
import { doFilmSort } from '../actions/filmSort';
import {  Badge } from 'react-bootstrap';
import { Link } from 'react-router-dom';

class FilmSort extends React.Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    this.props.fetchFilmSort();
  }

  render() {
    const arrGenre = this.props.filmSort.filter(function (item) {
      return item.genre === 'genre';
    });
    const arrAnnee = this.props.filmSort.filter(function (item) {
      return item.genre === 'annee';
    });

    return (
      <React.Fragment>
        <div className="mt-3" style={{ padding: '25px', border: '1px solid white' }}>
          <h4 style={{ fontWeight: 'bold' }}>Filtré par année de production</h4>
          {
            arrAnnee.map((item, idx) => {
              return (
                <React.Fragment key={idx}>
                  <Link variant="outline-secondary" 
                  className="mt-2 mr-2 btn btn-outline-secondary "  to ={`/search/filter/annee/${item.id}/${item.name}`}>
                    {item.name} <Badge variant="light"> {item.nbfilm} </Badge><br />
                  </Link>
                </React.Fragment>
              )
            })
          }
        </div>
        <div className="mt-3" style={{ padding: '25px', border: '1px solid white' }}>
          <h4 style={{ fontWeight: 'bold' }}>Filtré par genre</h4>
          {
            arrGenre.map((item, idx) => {
              return (
                <React.Fragment key={idx}>
                  <Link variant="outline-secondary" 
                  className="mt-2 mr-2 btn btn-outline-secondary " to ={`/search/filter/genre/${item.id}/${item.name}`}>
                    {item.name} <Badge variant="light"> {item.nbfilm} </Badge><br />
                  </Link>
                </React.Fragment>
              )
            })
          }

        </div>
      </React.Fragment>
    )
  }
}

const mapStatetoProps = state => {
  return {
    isLoading: state.filmSortState.isLoading,
    filmSort: state.filmSortState.filmSort,
    error: state.filmSortState.error
  };
};
const mapDispatchToProps = dispatch => ({
  fetchFilmSort: () => dispatch(doFilmSort())
});
export default connect(
  mapStatetoProps,
  mapDispatchToProps
)(FilmSort);


