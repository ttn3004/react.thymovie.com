import React from 'react';

class FilmListBestserie extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        const newArr = this.props.films.filter(function(item){
            return item.bests==='BESTSERIE';
        })
        return(
            <React.Fragment>
                <b>Liste des meilleures séries tout le temps </b>
                <ul>
                    {
                        newArr.map((item,idx)=>{
                            return(
                            <li key ={idx}>
                                {item.id} &nbsp; &nbsp; {item.title}
                            </li>
                            )
                        })
                    }
                </ul>
            </React.Fragment>
        )
    }
}
export default FilmListBestserie;