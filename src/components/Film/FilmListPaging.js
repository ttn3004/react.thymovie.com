import React from 'react'
import DataTablePagingComponent from './DataTablePagingComponent'
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import {doBreadcrumbs} from '../../actions/app';

class FilmListPaging extends React.Component {
    constructor(props) {
        super(props)
    }
    componentDidMount() {
        this.props.initBreadcrumbs([{name:'Home',url:'/'},{name:'Films',url:'/films', active:'true'}]);
    }
    render() {
        return (
            <div>

                <React.Fragment>

                    <DataTablePagingComponent type="film" />
                </React.Fragment>
            </div>
        )
    }
}
const mapDispatchToProps = dispatch => ({
    initBreadcrumbs:(arr) => dispatch(doBreadcrumbs(arr))
})
export default withRouter(connect(null,mapDispatchToProps)(FilmListPaging));