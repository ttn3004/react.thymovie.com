import React from 'react';

const FilmListBests = (props) => {
    const newArr= props.films.filter(function(item){
        return item.bests==="BESTS"
    });

    return (
        
        <React.Fragment>
            <b>Liste des meilleurs films dans tous les temps</b>
            <ul>
            
                {
                    newArr.map((item, idx)=>{
                        return (
                            <li key = {idx}>
                                {item.id} &nbsp; &nbsp; {item.title}
                            </li>
                        )
                    })
                }
            </ul>
        </React.Fragment>
    )
}

export default FilmListBests;