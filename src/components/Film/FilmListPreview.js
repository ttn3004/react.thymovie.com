import React from 'react';

const FilmListPreview = (props) => {
    var newArr = props.films.filter((item)=>{
        return item.bests === 'PREVIEW'
    });
    return (
        <React.Fragment>
            <b>Liste des films avant-premier</b>
            <ul>
                {
                    newArr.map((item, idx)=> {
                        return(
                           
                            
                            <li key={idx}>
                                
                                {item.id} &nbsp; &nbsp; {item.title}
                            </li>
                            
                        )
                    })
                }
            </ul>
        </React.Fragment>
    )
}
export default FilmListPreview;