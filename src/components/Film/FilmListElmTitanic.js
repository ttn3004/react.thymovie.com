import React from 'react';
import { Link } from 'react-router-dom';
import {Figure, Row, Col, Container} from 'react-bootstrap';



const FilmListElmTitanic = (props) => {

    return (
        <React.Fragment>
            {props.item && (
                <div>
                  
                    
                    <Figure>
                    <Row className="align-items-center">
                        <Col lg="2">
                        <Figure.Image
                            fluid={true}
                            
                            style={{maxWidth:'100%', height:'auto'}}
                            alt="250x250"
                            src={`http://thymovie.com/image/large/thymovies-img/${props.item.idcine}/${props.item.images_path}`}
                        />
                        </Col>
                        <Col lg="8">
            <p>{props.item.film_type_name}</p>
                        <Link to={`/films/${props.item.id}/${props.item.title}`} exact="true">
                        <h5>{props.item.title}</h5>
                    </Link>
            <p>{props.item.year_production}</p>
                        <Figure.Caption>
                            {props.item.synopsis}
                        </Figure.Caption>
                        </Col>
                        </Row>
                    </Figure>
                     <hr color="grey"/> 
                    
                    {/* <img
                        alt=""

                        variant="top"
                        src={`http://thymovie.com/image/large/thymovies-img/${props.item.idcine}/${props.item.images_path}`}
                    />
                    <p>{props.item.synopsis}</p> */}
                    
                </div>
            )}

        </React.Fragment>
    )
}
export default FilmListElmTitanic;