import React from 'react';
import { connect } from 'react-redux';
import { doFilmItem } from '../../actions/filmItem';
import { doAddToMyList } from '../../actions/app';
//import { Link } from 'react-router-dom';
import RoutesFilmItem from '../../routes/RoutesFilmItem';
import {
  Nav,
  Row,
  Col,
  Container,
  Image,
  Spinner,
  Dropdown,
  Button
} from 'react-bootstrap';
import { NavLink, withRouter } from 'react-router-dom';
import FilmItemTrailer from './FilmItemTrailer';
import FilmItemGal from './FilmItemGal';
import { doBreadcrumbs } from '../../actions/app';
import { faHeart } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

class FilmItem extends React.Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    //lay idItem bang this.props.match.params.item roi truyen vao fetchFilmItem()
    //sau do fetchFilmItem() se truyen xuong dispatch(doFilmItem(id))
    const idItem = this.props.match.params.item;
    //fetchFilmItem() la props cua FilmItem va duoc mapper boi store
    this.props.fetchFilmItem(idItem);

    if (this.props.match.path.startsWith('/series/'))
      this.props.initBreadcrumbs([
        { name: 'Home', url: '/' },
        { name: 'Séries', url: '/series' },
        { name: this.props.match.params.title, url: '#', active: true }
      ]);
    else
      this.props.initBreadcrumbs([
        { name: 'Home', url: '/' },
        { name: 'Films', url: '/films' },
        { name: this.props.match.params.title, url: '#', active: true }
      ]);
  }

  componentDidUpdate(prevProps) {
    const idItem = this.props.match.params.item;
    if (prevProps.match.params.item !== idItem) {
      this.props.fetchFilmItem(idItem);
      if (this.props.match.path.startsWith('/series/'))
        this.props.initBreadcrumbs([
          { name: 'Home', url: '/' },
          { name: 'Séries', url: '/series' },
          { name: this.props.match.params.title, url: '#', active: true }
        ]);
      else
        this.props.initBreadcrumbs([
          { name: 'Home', url: '/' },
          { name: 'Films', url: '/films' },
          { name: this.props.match.params.title, url: '#', active: true }
        ]);
    }
  }

  checkUserAuthValid = obj => {
    for (var key in obj) {
      //check xem co property trong obj json
      if (obj.hasOwnProperty(key)) {
        return true;
      }
    }
    return false;
  };

  isInWishlist_line = (arr, filmId) => {
    for (let i = 0; i < arr.length; i++) {
      const line = arr[i];
      for (let j = 0; j < line.wistlist_lines.length; j++) {
        const wishlist_line = line.wistlist_lines[j];

        if (filmId === wishlist_line.id) {
          return line.label;
        }
      }
    }
    return '';
  };

  render() {
    /** photos */
    const typeimage =
      this.props.filmItem.film_type_id === 12
        ? 'thymovies-serie-img'
        : 'thymovies-img';
    const sizeimage = this.props.filmItem.film_images ? 'med' : 'large';
    const arrImage = this.props.filmItem.film_images
      ? this.props.filmItem.film_images
      : this.props.filmItem.film_images_large;
    const idCine = this.props.filmItem.idcine;

    /** video */
    let sizevideo = null;
    let namevideo = null;
    try {
      if (Array.isArray(this.props.filmItem.film_videos)) {
        const objVideo = this.props.filmItem.film_videos[0];
        if (objVideo.vid_high && objVideo.vid_high !== '') {
          sizevideo = 'high';
          namevideo = objVideo.vid_high;
        } else if (objVideo.vid_med && objVideo.vid_med !== '') {
          sizevideo = 'medium';
          namevideo = objVideo.vid_med;
        }
      }
    } catch (error) {
      console.error(error);
    }

    // label wishlist
    let labelWl = '';
    if (this.props.userApp && Array.isArray(this.props.userApp.wishlists))
      labelWl = this.isInWishlist_line(
        this.props.userApp.wishlists,
        this.props.filmItem.id
      );

    return (
      <React.Fragment>
        {this.props.isLoading && (
          <Spinner
            as="span"
            animation="grow"
            role="status"
            aria-hidden="true"
            style={{
              width: '3rem',
              height: '3rem',
              position: 'fixed',
              zIndex: '999',
              overflow: 'visible',
              margin: 'auto',
              top: '0',
              left: '0',
              bottom: '0',
              right: '0'
            }}
          />
        )}
        {this.props.error ? (
          this.props.error
        ) : (
          <React.Fragment>
            {/* ajouter condition this.props.filmItem seulement si recevoir les données de backend, afficher tous sinon affiche rien */}
            {this.props.filmItem && (
              <React.Fragment>
                <Container fluid={true}>
                  {/* titre du film */}
                  <h1 className="text-center mt-5">
                    {this.props.filmItem.title}
                  </h1>
                  {sizevideo && namevideo && (
                    <FilmItemTrailer
                      idcine={idCine}
                      sizevideo={sizevideo}
                      namevideo={namevideo}
                    />
                  )}
                </Container>

                <Container className="my-5">
                  <Row>
                    <Col lg="8">
                      <Nav variant="tabs">
                        <Nav.Item>
                          <Nav.Link
                            as={NavLink}
                            to={`${this.props.match.url}/detail`}
                          >
                            Détail du film
                          </Nav.Link>
                        </Nav.Item>
                        <Nav.Item>
                          <Nav.Link
                            as={NavLink}
                            to={`${this.props.match.url}/synopsis`}
                          >
                            Synopsis
                          </Nav.Link>
                        </Nav.Item>

                        <Nav.Item>
                          <Nav.Link
                            as={NavLink}
                            to={`${this.props.match.url}/commentaires`}
                          >
                            Commentaires
                          </Nav.Link>
                        </Nav.Item>
                      </Nav>
                      <RoutesFilmItem
                        path={this.props.match.path}
                        //truyen prop filmItem cho RoutesFilmItem
                        filmItem={this.props.filmItem}
                      />
                    </Col>
                    <Col
                      lg="4"
                      style={{ padding: '25px', border: '1px solid white' }}
                    >
                      <div>
                        <Image
                          className="d-block ml-auto mr-auto "
                          fluid
                          src={
                            this.props.filmItem.film_type_id === 12
                              ? `http://thymovie.com/image/large/thymovies-serie-img/${this.props.filmItem.idcine}/${this.props.filmItem.images_path}`
                              : `http://thymovie.com/image/large/thymovies-img/${this.props.filmItem.idcine}/${this.props.filmItem.images_path}`
                          }
                        />
                        {this.checkUserAuthValid(this.props.userApp) && (
                          <React.Fragment>
                            {this.isInWishlist_line(
                              this.props.userApp.wishlists,
                              this.props.filmItem.id
                            ) === '' ? (
                              // si le film n'est pas dans une wishlist => apparaitre dropdown pour ajouter à la liste
                              <React.Fragment>
                                <Dropdown className="mt-3">
                                  <Dropdown.Toggle
                                    size="sm"
                                    id="dropdown-basic"
                                  >
                                    <FontAwesomeIcon
                                      size="sm"
                                      color="#fc26aa"
                                      icon={faHeart}
                                    />{' '}
                                    Ajouter à ma liste
                                  </Dropdown.Toggle>
                                  <Dropdown.Menu>
                                    {this.props.userApp.wishlists &&
                                      this.props.userApp.wishlists.map(
                                        (item, idx) => {
                                          return (
                                            <Dropdown.Item
                                              key={idx}
                                              onClick={() => {
                                                this.props.addToMyList(
                                                  this.props.filmItem.id,
                                                  item.id
                                                );
                                              }}
                                            >
                                              {item.label}
                                            </Dropdown.Item>
                                          );
                                        }
                                      )}
                                  </Dropdown.Menu>
                                </Dropdown>
                              </React.Fragment>
                            ) : (
                              // si le film est dans une wishlist => apparaitre bouton "ce film est dans {lenomdelaliste}"
                              <React.Fragment>
                                <Button
                                  style={{
                                    backgroundColor: '#fc26aa',
                                    borderColor: '#fc26aa'
                                  }}
                                  className="mt-3"
                                  size="sm"
                                >
                                  {' '}
                                  <FontAwesomeIcon
                                    size="sm"
                                    color="white"
                                    icon={faHeart}
                                  />
                                  &nbsp; Ce film est dans {labelWl}
                                </Button>
                              </React.Fragment>
                            )}
                          </React.Fragment>
                        )}
                      </div>
                    </Col>
                  </Row>
                  {Array.isArray(arrImage) && (
                    <FilmItemGal
                      idcine={idCine}
                      typeimage={typeimage}
                      sizeimage={sizeimage}
                      photos={arrImage}
                    />
                  )}
                </Container>
              </React.Fragment>
            )}
          </React.Fragment>
        )}
      </React.Fragment>
    );
  }
}
// conan
const mapStatetoProps = state => {
  return {
    isLoading: state.filmItemState.isLoading,
    filmItem: state.filmItemState.filmItem,
    error: state.filmItemState.error,
    userApp: state.appState.userApp
  };
};

//fetch duoc mapper voi doFilmItem de goi action
const mapDispatchToProps = dispatch => ({
  fetchFilmItem: id => dispatch(doFilmItem(id)),
  initBreadcrumbs: arr => dispatch(doBreadcrumbs(arr)),
  addToMyList: (idFilm, idWl) => dispatch(doAddToMyList(idFilm, idWl))
});
export default withRouter(
  connect(mapStatetoProps, mapDispatchToProps)(FilmItem)
);

/* cucuc */
