import React from 'react';
import { Container } from 'react-bootstrap';

const FilmItemTrailer = (props) => {
    return (
        <React.Fragment>
           <Container
           fluid={true} 
           className="my-3">
            <div  className="embed-responsive embed-responsive-16by9 ">
                <iframe className="embed-responsive-item m-auto" title="traillerVid"
                    src={`http://thymovie.com/videos/thymovies-vid/${props.idcine}/${props.sizevideo}/${props.namevideo}`} allowFullScreen>
                </iframe>
            </div>

            </Container>
        </React.Fragment>
    )
};

export default FilmItemTrailer;