import React from 'react';
import { Link } from 'react-router-dom';



const FilmListElement = (props) => {
    
    
    return (
        <React.Fragment>
            
            {props.item  &&(
                <div>
                    <Link to={`/films/${props.item.id}/${props.item.title}`} exact="true">
                        <h1>{props.item.title}</h1>
                    </Link>
                    
                    <img        
                                src={`http://thymovie.com/image/large/thymovies-img/${props.item.idcine}/${props.item.images_path}`}>
                               </img> 
                </div>
            )}


        </React.Fragment>
    )

}
export default FilmListElement;