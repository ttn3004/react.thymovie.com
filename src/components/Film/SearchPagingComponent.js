import React from 'react';
import ReactTable from 'react-table';
import 'react-table/react-table.css';
// import './datatable.css'
import { connect } from 'react-redux';
import { Container, Row, Col } from 'react-bootstrap';
import { doFilmsPaging } from '../../actions/filmList';
import { Link } from 'react-router-dom';
import { withRouter } from 'react-router-dom';

import FilmSort from '../FilmSort';

class SearchPagingComponent extends React.Component {
  constructor(props) {
    super(props);
    let postObj = {};
    if (this.props.findingsearch === 'kw') {
      postObj = {
        ...this.props.postDataTableObject,
        filmType: this.props.type || 'all',
        listFiltered: [],
        searchByKeyword: this.props.searchByKeyword
      };
    } else if (this.props.findingsearch === 'filter') {
      postObj = {
        ...this.props.postDataTableObject,
        filmType: this.props.type || 'all',
        listFiltered: [],
        searchByKeyword:'',
        menuFiltered: [
          {
            id: this.props.filtername,
            value: this.props.filterid
          }
        ]
      };
    }
    this.state = { postDataTableObject: postObj };
  }
  componentDidMount() {
    this.props.fetchFilmsPaging(this.state.postDataTableObject);
  }
  componentDidUpdate(prevProps) {
 
    if (
      this.props.findingsearch === 'kw' ||
      this.props.findingsearch === 'filter'
    ) {
      if (this.props.match.path.includes('/keyword/')) {
        const oldKw = prevProps.searchByKeyword;
        const searchByKeyword = this.props.searchByKeyword;
        if (oldKw !== searchByKeyword) {
          const postObj = {
            ...this.props.postDataTableObject,
            listFiltered: [],
            menuFiltered:[],
            searchByKeyword: searchByKeyword
          };
          this.setState({
            postDataTableObject: postObj
          });
          this.props.fetchFilmsPaging(postObj);
        }
      } else if (this.props.match.path.includes('/filter/')) {
        const oldFilterid = prevProps.match.params.filterid;
        const newFilterid = this.props.match.params.filterid;
        const newFiltername = this.props.match.params.filtername;
        if (oldFilterid !== newFilterid) {
          const postObj = {
            ...this.props.postDataTableObject,
            filmType: this.props.type || 'all',
            listFiltered: [],
            searchByKeyword:'',
            menuFiltered: [
              {
                id: newFiltername,
                value: newFilterid
              }
            ]
          };
          this.setState({
            postDataTableObject: postObj
          });
          this.props.fetchFilmsPaging(postObj);
        }
      }
    } 
  }

  refetchFilmsPaging = (page, pageSize, sorted, filtered) => {
    let postObject = {
      ...this.state.postDataTableObject,
      filmType: this.props.type,
      listFiltered: [],
      page: page,
      pageSize: pageSize,
      sorted: sorted,
      filtered: filtered
    };
    this.props.fetchFilmsPaging(postObject);
  };
  //Colonnes de la table
  columns = () => [
    {
      Header: '',
      accessor: 'image',
      maxWidth: 400,
      Cell: row => {
        const sizeImg = row.original.images_path ? 'med' : 'large';
        const typeImg =
          row.original.film_type_id === 12
            ? 'thymovies-serie-img'
            : 'thymovies-img';
        return (
          <div>
            <img
              alt=""
              src={`http://thymovie.com/image/${sizeImg}/${typeImg}/${row.original.idcine}/${row.original.images_path}`}
            />
          </div>
        );
      }
    },

    {
      Header: '',
      accessor: 'title',
      filterable: true,
      Cell: row => {
        let urlFilm = 'films';
        if (row.original.film_type_id === 12) urlFilm = 'series';
        return (
          <div>
            <Link
              className="text-white"
              to={`/${urlFilm}/${row.original.id}/${row.original.title}`}
              exact="true"
            >
              <h4>{row.original.title}</h4>
            </Link>
            <p className="text-muted">
              Année de production: {row.original.year_production}
            </p>
            <div style={{ whiteSpace: 'pre-wrap' }}>
              <p className="text-justify text-light">{row.original.synopsis}</p>
            </div>
          </div>
        );
      }
    }
  ];
  render() {
    return (
      <Container fluid={true}>
        <Row>
          <Col md={8}>
            <ReactTable
              data={this.props.postDataTableObject.listFiltered}
              pages={this.state.postDataTableObject.totalPage}
              columns={this.columns()}
              defaultPageSize={10}
              className="-striped -highlight"
              loading={this.props.isLoading}
              showPagination={true}
              showPaginationTop={false}
              showPaginationBottom={true}
              pageSizeOptions={[5, 10, 20, 25, 50, 100]}
              manual // this would indicate that server side pagination has been enabled
              onFetchData={(state, instance) => {
                this.refetchFilmsPaging(
                  state.page,
                  state.pageSize,
                  state.sorted,
                  state.filtered
                );
              }}
              // Text
              previousText="Previous"
            />
          </Col>
          <Col md={4}>
            <FilmSort />
          </Col>
        </Row>
      </Container>
    );
  }
}
const mapStatetoProps = state => {
  return {
    isLoading: state.filmListState.isLoading,
    postDataTableObject: state.filmListState.postDataTableObject,
    error: state.filmListState.error
  };
};
const mapDispatchToProps = dispatch => ({
  fetchFilmsPaging: postObject => dispatch(doFilmsPaging(postObject))
});
export default withRouter(
  connect(mapStatetoProps, mapDispatchToProps)(SearchPagingComponent)
);
