import React from 'react';

class FilmListAtCine extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        //phai viet o render vi khi thang bo thay doi thi thang con se update theo
        const newArr= this.props.films.filter(function(item){
            return item.bests==='ATCINEMA';
        })
        return(

            <React.Fragment>
                <b>Liste des films actuels en cinéma</b>
                <ul>
                    {
                        newArr.map((item,idx)=>{
                            return(
                                <li key={idx}>
                                    {item.id} &nbsp; &nbsp; {item.title}
                                </li>
                            )
                        })
                    }
                </ul>
            </React.Fragment>
        )
    }
}

export default FilmListAtCine;