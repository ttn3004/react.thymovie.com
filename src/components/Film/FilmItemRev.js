import React from 'react';
import { Container, Alert, Row, Col } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPaperPlane } from '@fortawesome/free-solid-svg-icons';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import FormControl from 'react-bootstrap/FormControl';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { doFilmItemRev } from '../../actions/filmItem';
import moment from 'moment';

class FilmItemRev extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
            rev:''
        }
    }
    checkUserAuthValid = obj => {
        for (var key in obj) {
            //check xem co property trong obj json
            if (obj.hasOwnProperty(key)) {
                return true;
            }
        }
        return false;
    };
    handleRev = () => {
        //chi thuc hien fonc khi co commentaire dans input
        if(this.state.rev.length>0)
        this.props.filmItemRev(
            this.state.rev,
            this.props.filmItem.id
        )
    }
    handleChangeRev = (event) => {
        this.setState({ rev: event.target.value })
    }
    render() {
        return (
            <React.Fragment>
                <Container style={{ height: '380px', overflow: 'auto' }} className="my-3">
                    {this.checkUserAuthValid(this.props.userApp) ? (
                        <Container style={{ overflow: 'auto' }} >
                            <h4> Publier mes commentaires</h4>
                            <Form >
                                <FormControl
                                    size="lg"
                                    className="my-2"
                                    style={{
                                        border: '1px solid grey',
                                        backgroundColor: '#1d2e59',
                                        color: 'white'
                                    }}
                                    as="textarea" aria-label="With textarea"
                                    onChange={this.handleChangeRev.bind(this)}
                                />
                                <Button
                                    className="mb-1"
                                    onClick={this.handleRev}
                                    style={{
                                        display: 'block',
                                        marginLeft: 'auto',
                                    }} variant="primary">
                                    Envoyer&nbsp;
                                <FontAwesomeIcon color="#fc26aa" icon={faPaperPlane} 
                                //size="md" 
                                />
                                </Button>
                            </Form>

                        </Container>) : (
                            <Container>
                                <Alert  variant="info"> Connectez-vous pour poster vos commentaires </Alert>
                            </Container>
                        )
                    }

                    {this.props.filmItem.film_reviews ?
                        (<Container>
                            <h4 > Commentaires </h4>
                            {/* <hr color="#fc26aa"/> */}
                            <hr color="#34dbf7"/>
                            {this.props.filmItem.film_reviews.map((item, idx) => {
                                return (
                                    <Row key={idx}>

                                        <Col md={3}> <h6>{item.name}</h6></Col>
                                        <Col md={9}>
                                            <em style={{ fontSize: '12px' }} className="text-muted">Publié {moment(item.updated_at, 'YYYY-MM-DD HH:mm').format('DD/MM/YY HH:mm')}</em> <br />
                                            <p style={{ fontSize: '14px' }} className="text-justify font-weight-light">{item.comments}</p>
                                        </Col>

                                    </Row>
                                )
                            })}</Container>) : ('')
                    }
                </Container>
            </React.Fragment>
        );
    };
}


const mapStatetoProps = state => {
    return {
        isLoading: state.appState.isLoading,
        userApp: state.appState.userApp,
        error: state.appState.error
    };
};
const mapDispatchToProps = dispatch => ({
    filmItemRev: (rev, idFilm) => dispatch(doFilmItemRev(rev, idFilm)),
});


export default withRouter(connect(mapStatetoProps, mapDispatchToProps)(FilmItemRev));
