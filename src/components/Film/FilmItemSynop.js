import React from 'react';
import {Container} from 'react-bootstrap';

const FilmItemSynop = (props) => {
    return (
       <React.Fragment >
        <Container className="my-3" >
        <p  className="text-justify "> {props.filmItem.synopsis} </p>
        </Container>
        </React.Fragment>
    );
};

export default FilmItemSynop;