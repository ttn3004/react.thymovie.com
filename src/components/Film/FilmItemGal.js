import React from 'react';
import { Row, Col, Image, Container } from 'react-bootstrap';

class FilmItemGal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      photos: props.photos
    };
  }

  render() {
    return (
      <React.Fragment>
        
        {this.state.photos && (
          <Container className="my-5">
            <h4 className="text-center">Galerie de photos</h4>
            <Row noGutters className="mt-3">
            
              {this.state.photos.map((item, index) => {
                return (
                  <Col key={index}   style={{padding: '5px 5px 5px 5px'}}  
                   xs={12}
                   sm={6}
                   md={4}
                   lg={3}
                   >
                    <Image fluid style={{objectFit: 'fill'}}  width="100%" height="300"
                    
                      src={`http://thymovie.com/image/${this.props.sizeimage}/${this.props.typeimage}/${this.props.idcine}/${item.images_path}`}
                    />
                  </Col>
                );
              })}
            </Row>
          </Container>
        )}
      </React.Fragment>
    );
  }
}

export default FilmItemGal;
