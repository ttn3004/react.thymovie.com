import React from 'react';
import { Row, Col, Container } from 'react-bootstrap';
import { withRouter } from 'react-router-dom';

class FilmItemDetail extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
    }

    render() {
        return (
            <React.Fragment>
                <Container className="my-3">
                    <Row className="my-2">
                        <Col
                            lg={3}
                            md={3}>
                            <em className="text-muted">Titre original</em>
                        </Col>
                        {this.props.filmItem.original_title ?
                            <Col
                                lg={6}
                                md={6}>
                                {this.props.filmItem.original_title}</Col>
                            : <Col
                                lg={6}
                                md={6}> <em className="text-muted" >updating</em></Col>
                        }
                    </Row>


                    <Row className="my-2">
                        <Col lg={3} md={3}>
                            <em className="text-muted">Réalisation </em> </Col>
                        {Array.isArray(this.props.filmItem.director) && this.props.filmItem.director.length > 0 ?

                            <Col lg={6} md={6}>
                                {this.props.filmItem.director && (
                                    this.props.filmItem.director.map((item, idx) => {
                                        return (
                                            <span key={idx}>
                                                {item.name}
                                            </span>
                                        )
                                    }))
                                }
                            </Col>
                            :
                            <Col lg={6} md={6}><em className="text-muted" > updating</em> </Col>}
                    </Row>



                    <Row className="my-2">
                        <Col lg={3} md={3}>
                            <em className="text-muted">Acteurs principaux</em>
                        </Col>
                        {Array.isArray(this.props.filmItem.actor) && this.props.filmItem.actor.length > 0 ?

                            <Col lg={6} md={6}>
                                {this.props.filmItem.actor && (
                                    this.props.filmItem.actor.map((item, idx) => {
                                        return (
                                            <span key={idx}>
                                                {item.name}&nbsp;
                                <br />
                                            </span>
                                        )
                                    }))
                                }
                            </Col>
                            :
                            <Col lg={6} md={6}> <em className="text-muted" >updating</em> </Col>}</Row>





                    <Row className="my-2">
                        <Col lg={3} md={3}>
                            <em className="text-muted">Pays d'origine</em>
                        </Col>
                        {this.props.filmItem.country ?
                            <Col lg={6} md={6}>

                                {this.props.filmItem.country && (
                                    this.props.filmItem.country.map((item, idx) => {
                                        return (
                                            <span key={idx}>
                                                {item.name}&nbsp;
                        </span>

                                        )
                                    }))
                                }
                            </Col> : <Col lg={6} md={6}> <em className="text-muted" >updating</em> </Col>}
                    </Row>




                    <Row className="my-2">
                        <Col lg={3} md={3}>
                            <em className="text-muted"> Année de production</em> </Col>
                        <Col lg={6} md={6}>
                            {this.props.filmItem.year_production} </Col>
                    </Row>




                    <Row className="my-2">
                        <Col lg={3} md={3}><em className="text-muted">Genre </em> </Col>
                        <Col lg={6} md={6}>{this.props.filmItem.genre && (
                            this.props.filmItem.genre.map((item, idx) => {
                                return (
                                    <span key={idx}>
                                        {item.name}
                                        &nbsp; <br />
                                    </span>
                                )
                            })
                        )
                        }
                        </Col>
                    </Row>



                    <Row className="my-2">
                        <Col lg={3} md={3}>
                            <em className="text-muted"> Durée </em> </Col>
                        <Col lg={6} md={6}>{this.props.filmItem.duration} </Col>
                    </Row>

                </Container>
            </React.Fragment>
        )

    }
}

export default withRouter(FilmItemDetail);