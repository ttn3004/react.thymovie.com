import React from 'react';
import { connect } from 'react-redux';
import { doFilmList } from '../actions/filmList';
import { getFilmListSelector } from '../selectors/film';
//import FilmListElement from './Film/FilmListElement';
import CarouselFactory from './Carousel/CarouselFactory';
import { Container, Spinner, Fade } from 'react-bootstrap';
import { doBreadcrumbs } from '../actions/app';
import { withRouter } from 'react-router-dom';

const spaceTitle = {
  fontWeight: 'bold',
  color: 'white',
  paddingTop: '30px',
  paddingLeft: '5px',
  paddingRight: '5px'
};

class Home extends React.Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    if (this.props.filmList.length === 0) {
      this.props.fetchFilmMenuHome();
    }
    this.props.initBreadcrumbs([{ name: 'Home', url: '/', active: 'true' }]);
  }

  filterFilmList(filmList, typeFilm) {
    return filmList.filter(function(item) {
      return item.bests === typeFilm;
    });
  }

  getListForCarousel(filmList, nbElem) {
    let arrRes = [];

    const arrAtCine = this.filterFilmList(filmList, 'ATCINEMA');
    const arrforCar = arrAtCine.slice(0, nbElem);
    arrforCar.map(function(elem) {
      arrRes.push(elem);
    });

    const arrPre = this.filterFilmList(filmList, 'PREVIEW');
    const arrforCar2 = arrPre.slice(0, nbElem);
    arrforCar2.map(function(elem) {
      arrRes.push(elem);
    });

    const arrBests = this.filterFilmList(filmList, 'BESTS');
    const arrforCar3 = arrBests.slice(0, nbElem);
    arrforCar3.map(function(elem) {
      arrRes.push(elem);
    });

    return arrRes;
  }

  render() {
    const filmsAtCine = this.props.filmList.filter(function(item) {
      return item.bests === 'ATCINEMA';
    });
    const filmsPre = this.props.filmList.filter(function(item) {
      return item.bests === 'PREVIEW';
    });
    const filmsBests = this.props.filmList.filter(function(item) {
      return item.bests === 'BESTS';
    });
    const filmsBestSerie = this.props.filmList.filter(function(item) {
      return item.bests === 'BESTSERIE';
    });
    return (
      //style={{  display: 'block', }}
      <React.Fragment>
        {this.props.isLoading && (
          <Spinner
            as="span"
            animation="grow"
            role="status"
            aria-hidden="true"
            style={{
              width: '3rem',
              height: '3rem',
              position: 'fixed',
              zIndex: '999',
              overflow: 'visible',
              margin: 'auto',
              top: '0',
              left: '0',
              bottom: '0',
              right: '0'
            }}
          />
        )}
        {this.props.error ? (
          this.props.error
        ) : (
          <React.Fragment>
            {Array.isArray(this.props.filmList) &&
              this.props.filmList.length > 0 && (
                <Container fluid={true} className="pl-1 pr-1" transition={Fade}>
                  {this.props.filmList.length > 0 && (
                    <CarouselFactory
                      filmList={this.getListForCarousel(this.props.filmList, 4)}
                      count={4}
                    ></CarouselFactory>
                  )}
                  <Container style={{ backgroundColor: '' }}>
                    <h4 style={spaceTitle}>Au cinéma</h4>
                    {this.props.filmList.length > 0 && (
                      <CarouselFactory
                        filmList={filmsAtCine}
                        count={6}
                      ></CarouselFactory>
                    )}

                    <h4 style={spaceTitle}> Avant-premières </h4>
                    {this.props.filmList.length > 0 && (
                      <CarouselFactory
                        filmList={filmsPre}
                        count={6}
                      ></CarouselFactory>
                    )}

                    <h4 style={spaceTitle}>
                      Meilleurs films de tous les temps
                    </h4>
                    {this.props.filmList.length > 0 && (
                      <CarouselFactory
                        filmList={filmsBests}
                        count={6}
                      ></CarouselFactory>
                    )}

                    <h4 style={spaceTitle}>
                      Meilleurs séries de tous les temps{' '}
                    </h4>
                    {this.props.filmList.length > 0 && (
                      <CarouselFactory
                        filmList={filmsBestSerie}
                        count={6}
                      ></CarouselFactory>
                    )}
                  </Container>
                </Container>
              )}
          </React.Fragment>
        )}
      </React.Fragment>
    );
  }
}

const mapStatetoProps = state => {
  return {
    isLoading: state.filmListState.isLoading,
    filmList: getFilmListSelector(state.filmListState),
    error: state.filmListState.error
  };
};
const mapDispatchToProps = dispatch => ({
  fetchFilmMenuHome: () => dispatch(doFilmList()),
  initBreadcrumbs: arr => dispatch(doBreadcrumbs(arr))
});
export default withRouter(connect(mapStatetoProps, mapDispatchToProps)(Home));
