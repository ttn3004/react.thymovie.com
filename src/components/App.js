import React from 'react';
import './App.css';
import Header from './Header';
import Routes from '../routes/Routes';
import 'bootstrap/dist/css/bootstrap.min.css';
import {doUserAuth} from '../actions/app';
import {connect} from 'react-redux';
import Footer from './Footer';
import Breadcrumbs  from './Breadcrumbs';


class App extends React.Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    this.props.fetchUserAuth();
    
  }
  render() {
    return (
     
      <div style={{paddingTop:'50px'}} >
       
        <Header/>

        <Breadcrumbs links={this.props.links} />
        
        <Routes userAuth={this.props.userApp}/>

        <Footer/>

      </div>
      
    )
  }
}

const mapStatetoProps = state => {
  return {
    isLoading: state.appState.isLoading,
    userApp: state.appState.userApp,
    error: state.appState.error,
    links: state.appState.links
  };
};
const mapDispatchToProps = dispatch => ({
  fetchUserAuth: () => dispatch(doUserAuth())
});
export default connect(
  mapStatetoProps,
  mapDispatchToProps
)(App);

