import React from 'react';
import { withRouter, Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAngleRight, faHome } from '@fortawesome/free-solid-svg-icons';


const styleBr = {
    color: 'white',
    // display: 'inline',

}

class Breadcrumbs extends React.Component {
    constructor(props) {
        super(props);
        


    }

    render() {
        return (
            <div>
                {this.props.links.map((item, idx) => {
                    return (
                        <React.Fragment key={idx} >
                            {item.name === 'Home' ?
                                (<Link to={item.url} style={styleBr} > <FontAwesomeIcon style={{ marginTop: '2rem', marginLeft: '2rem' }} icon={faHome} size="xs" /> {item.name}
                                    <FontAwesomeIcon className="mx-2" icon={faAngleRight} size="xs" /> </Link>) :
                                item.active ?
                                    (<Link to={item.url} style={{textDecoration: 'none'   }} className="text-white-50" > {item.name}
                                    </Link>) :
                                    (<Link to={item.url} style={styleBr}> {item.name}
                                        <FontAwesomeIcon className="mx-2" icon={faAngleRight} size="xs" />
                                    </Link>)
                            }
                        </React.Fragment>
                    )
                })}

            </div>

        )

    }
}

export default withRouter(Breadcrumbs);
