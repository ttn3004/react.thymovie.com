import React from 'react';
import { Route, Switch } from 'react-router-dom';
import FilmItemDetail from '../components/Film/FilmItemDetail';
import FilmItemSynop from '../components/Film/FilmItemSynop';
import FilmItemRev from '../components/Film/FilmItemRev';

const RoutesFilmItem = props => {

  return (
    <Switch>
      <Route
        exact={true}
        path={`${props.path}/`}
        render={rest => {
          return <FilmItemDetail {...rest} filmItem={props.filmItem} />;
        }}
      />

      <Route
        exact={true}
        path={`${props.path}/detail`}
        render={rest => {
          return <FilmItemDetail {...rest} filmItem={props.filmItem} />;
        }}
      />

      <Route
        exact={true}
        path={`${props.path}/synopsis`}
        render={rest => {
          return <FilmItemSynop {...rest} filmItem={props.filmItem} />;
        }}
      />

      <Route
        exact={true}
        path={`${props.path}/commentaires`}
        render={rest => {
          return <FilmItemRev {...rest} filmItem={props.filmItem} />;
        }}
      />
    </Switch>
  );
};
export default RoutesFilmItem;
