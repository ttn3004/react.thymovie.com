import React from 'react';
import { Route, Redirect, Switch } from 'react-router-dom';
import Home from '../components/Home';
import FilmList from '../components/FilmList';
import FilmSort from '../components/FilmSort';
import FilmItem from '../components/Film/FilmItem';
import Login from '../components/Login';
import Protected from '../components/Protected';
import PrivateRoute from './PrivateRoute';
import FilmListPaging from '../components/Film/FilmListPaging';
import SerieListPaging from '../components/Serie/SerieListPaging';
import SearchListPaging from '../components/Search/SearchListPaging';

const Routes = props => {
  const checkIsUserAuth = obj => {
    for (var key in obj) {
      //check xem co property trong obj json
      if (key==='id' && obj.hasOwnProperty(key)) return true;
    }
    return false;
  };

  return (
    //navigateur web muon hien nhu the nao (hien bang front) defini trong Routes
    <div>
      <Switch>
        <Route exact={true} path="/" component={Home} />
        <Route exact={true} path="/filmList" component={FilmList} />
        {/* <Route exact={true} path="/filmItem" component={FilmItem} /> */}
        <Route exact={true} path="/filmSort" component={FilmSort} />
        <Route
          path="/films/:item/:title"
          render={props => {
            return <FilmItem {...props} />;
          }}
        />
        <Route
          path="/series/:item/:title"
          render={props => {
            return <FilmItem {...props} />;
          }}
        />
        <Route
          exact={true}
          path="/films"
          render={props => {
            return <FilmListPaging {...props} />;
          }}
        />
        <Route
          exact={true}
          path="/series"
          render={props => {
            return <SerieListPaging {...props} />;
          }}
        />
         <Route
          exact={true}
          path="/search/keyword/:keyword"
          render={props => {
            return <SearchListPaging {...props} />;
          }}
        />
         <Route
          exact={true}
          path="/search/filter/:filtername/:filterid/:filtervalue"
          render={props => {
            return <SearchListPaging {...props} />;
          }}
        />
        <Route
          {...props} //prendre toutes les props de composant parent (props cua App truyen cho)
          path="/login"
          render={p =>
            checkIsUserAuth(props.userAuth) ? (
              <Redirect to="/protected" />
            ) : (
              <Login {...p} />
            )
          }
        />
        <PrivateRoute
          path="/protected"
          component={Protected}
          userAuth={props.userAuth}
          exact
        />
      </Switch>
    </div>
  );
};

export default Routes;
