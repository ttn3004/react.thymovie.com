import React from 'react';
import { Route, Redirect } from 'react-router-dom';

const PrivateRoute = ({ component: Component, ...rest }) => {
  const userAuth = rest.userAuth;

  const checkUserAuth = obj => {
    for (var key in obj) {
      //check xem co property trong obj json
      if (key === 'id' && obj.hasOwnProperty(key)) return true;
    }

    return false;
  };
  return (
    <Route
      {...rest}
      render={(
        props //props: history, location tu Route truyen chu  ko phai userApp
      ) =>
        checkUserAuth(userAuth) ? (
          //composant do composant parent (Route) dua cho
          <Component {...props} />
        ) : (
          <Redirect to="/login" />
        )
      }
    />
  );
};
export default PrivateRoute;
