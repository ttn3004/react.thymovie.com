import { axiosAuth } from './app';
import { TOKEN } from '../constants/config';
import axios from 'axios';
import { API_HOST } from '../constants/config';
import { axiosForPost, axiosForDelete } from './app';

const USER_LOGIN = 'login.php';
const USER_INFO = 'login.php?jwt=';
const myheaders = {
  'content-type': 'application/x-www-form-urlencoded;charset=utf-8'
};
const USER_REGISTER = 'spectator.php';
const USER_SAVE_WISHLIST = 'wishlist.php';
const DELETE_TO_LIST = 'wishlist_line.php';

//login
const axiosLogin = axios.create({
  baseURL: API_HOST,
  timeout: 15000,
  headers: myheaders
});
export const loginUserApi = (email, pass) => {
  const param = {
    email: email,
    password: pass
  };
  try {
    return axiosLogin.post(USER_LOGIN, param).then(res => {
      const token = res.data.jwt;
      //if token empty  t==> throw error avec msg = res.data.message
      if (typeof token === 'undefined') {
        throw new Error(res.data.message);
      } else {
        //cacher token dans browser __ TOKEN: var; token : data
        localStorage.setItem(TOKEN, token);
        return token;
      }
    });
  } catch (error) {
    return error;
  }
};

//register
const axiosRegister = axios.create({
  baseURL: API_HOST,
  timeout: 15000,
  headers: myheaders
});
export const registerUserApi = (email, pass) => {
  const param = {
    email: email,
    password: pass
  };
  try {
    return axiosRegister.post(USER_REGISTER, param).then(res => {
      const token = res.data.jwt;
      if (typeof token === 'undefined') throw new Error(res.data.message);
      //cacher token dans browser __ TOKEN: var; token : data
      localStorage.setItem(TOKEN, token);

      return token;
    });
  } catch (error) {
    return error;
  }
};

//logout
export const logoutUserApi = () => {
  localStorage.removeItem(TOKEN);
  sessionStorage.removeItem(TOKEN);
};

//save wishlist
export const userSaveWishListApi = name => {
  try {
    const token = localStorage.getItem(TOKEN);
    const data = {
      jwt: token,
      newWl_label: name
    };
    return axiosForPost
      .post(USER_SAVE_WISHLIST, JSON.stringify(data))
      .then(res => res.data.data);
  } catch (error) {
    return error;
  }
};

//delete wishlist
export const deleteWishlistApi = idWl => {
  try {
    const token = localStorage.getItem(TOKEN);
    const data = {
      jwt: token,
      idWishlist: idWl
    };
    return axiosForDelete
      .delete(USER_SAVE_WISHLIST, { data: data })
      .then(res => res.data.data);
  } catch (error) {
    return error;
  }
};

//add to my list
export const addToMyListApi = (idFilm, idWl) => {
  try {
    const token = localStorage.getItem(TOKEN);
    const data = {
      jwt: token,
      idFilm: idFilm,
      idWistlist: idWl
    };
    return axiosForPost
      .post(DELETE_TO_LIST, JSON.stringify(data))
      .then(res => res.data.data);
  } catch (error) {
    return error;
  }
};

//delete film from wishlist
export const deleteToMyListApi = idWl_line => {
  try {
    const token = localStorage.getItem(TOKEN);
    const data = {
      jwt: token,
      idWistlist_Line: idWl_line
    };
    return axiosForDelete
      .delete(DELETE_TO_LIST, { data: data })
      .then(res => res.data.data);
  } catch (error) {
    return error;
  }
};

const getUserInfo = tk => {
  try {
    //get gui token len cho server de lay info de user
    return (
      axiosAuth
        .get(USER_INFO + tk)
        //server gui user info ve
        .then(res => res.data.data)
    );
  } catch (error) {
    return error;
  }
};

//goi khi vao app (goi o App.js chu ko goi o Login.js)
export const userAuthApi = () => {
  //reprendre TOKEN da caché luc nay trong browser
  const token = localStorage.getItem(TOKEN);
  try {
    return getUserInfo(token);
  } catch (error) {
    return error;
  }
};
