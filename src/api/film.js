import {axiosInstance} from './app';
import {axiosForPost} from './app';
import {TOKEN} from  '../constants/config';

const FILM_MENUHOME = 'film.php/menuHome';
export const filmListMenuHomeApi = () => {
    try {
       return axiosInstance.get(FILM_MENUHOME).then(res => res.data);
    } catch (error) {
        return error;
    }
};

const FILM_SORT= 'configsort.php/film';
export const filmSortApi =() => {
    try {
        return axiosInstance.get(FILM_SORT)
        .then(res => res.data);
    }catch (error){
        return error;
    }
}

const FILM_ITEM = 'film.php/';
export const filmItemApi = (idItem) => {
    try {
        return axiosInstance.get(FILM_ITEM + idItem) 
        .then(res => res.data);
    }catch (error) {
        return error;
    }
}

const FILM_ITEM_REV = 'review.php';
export const filmItemRevApi = (rev,idFilm) => {
    try {
        const token = localStorage.getItem(TOKEN);
        const data={
            jwt:token,
            idFilm:idFilm,
            comment:rev
        }
        return axiosForPost.post(FILM_ITEM_REV, JSON.stringify(data))
        .then(res => res.data.data);
        
    } catch (error) {
        return error;
    }
}



const FILM_KEY_TITANIC = 'film.php/keyword/titanic';
export const filmKeyTitanicApi = () => {
    try {
        return axiosInstance.get(FILM_KEY_TITANIC)
        .then(res => res.data);
    } catch (error) {
        return error;
    }
}

const FILM_PAGING = 'film_datatable.php';
export const filmsPagingApi = (postObject) =>{
    try {
        return axiosForPost.post(FILM_PAGING,postObject)
        .then(res => res.data);
    } catch (error) {
        return error;
    }
}
/** serach on header typeahead */
const FILM_SEARCH_KEYWORD = 'film.php/keyword/';
export const filmSearchByKwApi = (query) => {
    try {
        return axiosInstance.get(FILM_SEARCH_KEYWORD+query)
        .then(res => res.data);
    } catch (error) {
        return error;
    }
}