import axios from 'axios';
import { API_HOST } from '../constants/config';

const myheaders= {
  'content-type': 'application/x-www-form-urlencoded;charset=utf-8'
}
export const axiosInstance = axios.create({
  baseURL: API_HOST,
  timeout: 15000
});

export const axiosForPost = axios.create({
  baseURL: API_HOST,
  timeout: 15000,
  headers:myheaders
});

export const axiosForDelete = axios.create({
  baseURL: API_HOST,
  timeout: 15000,
  headers: myheaders
})

export const axiosAuth = axios.create({
  baseURL: API_HOST,
  timeout: 15000
});
