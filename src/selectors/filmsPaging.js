import { createSelector } from 'reselect';

const filmList = state => state.postDataTableObject.listFiltered;

export const getFilmListSelector= createSelector (
    [ filmList ],
    (filmList) => filmList.map(f=>{
        //return ({name:p.name});
        return f;
    })
)