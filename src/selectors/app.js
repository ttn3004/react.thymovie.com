import {createSelector} from 'reselect';

const userApp = state => state.userApp;
export const getUserAppSelector= createSelector (
    [userApp],
    (userApp) =>userApp
);