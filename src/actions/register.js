import {
  APP_REGISTER_START,
  APP_REGISTER_SUCCESS,
  APP_REGISTER_FAILURE,
  APP_AUTH_SUCCESS
} from '../constants/actionTypes';
import { registerUserApi } from '../api/user';
import { userAuthApi } from '../api/user';

const doUserRegisterStart = () => {
  return {
    type: APP_REGISTER_START,
    error: ''
  };
};
const doUserRegisterSuccess = data => {
  return {
    type: APP_REGISTER_SUCCESS,
    data: data,
    error: ''
  };
};
const doUserRegisterFailure = error => {
  return {
    type: APP_REGISTER_FAILURE,
    error
  };
};

const doUserAuthSuccess = data => {
  return {
    type: APP_AUTH_SUCCESS,
    data,
    error: ''
  };
};

export const doUserRegister = (email, pass) => async dispatch => {
  dispatch(doUserRegisterStart());
  try {
    if (email.length <= 3 || pass.length < 3) {
      dispatch(doUserRegisterFailure('Formulaire non valide'));
    } else {
      const data = await registerUserApi(email, pass);
      //getUserInfo() de lay infoUser tu tk
      const dataUser = await userAuthApi();
      dispatch(doUserRegisterSuccess(data));
      dispatch(doUserAuthSuccess(dataUser));
    }
  } catch (error) {
    dispatch(doUserRegisterFailure(error.message));
  }
};
