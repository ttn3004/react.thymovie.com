import {FILM_KEY_TITANIC_START, 
    FILM_KEY_TITANIC_SUCCESS,
    FILM_KEY_TITANIC_FAILURE}
from '../constants/actionTypes';
import {filmKeyTitanicApi} from '../api/film' ;

const doFilmKeyTitanicStart = () => {
    return {
        type : FILM_KEY_TITANIC_START
    }
}
const doFilmKeyTitanicSuccess = (data) => {
    return {
        type : FILM_KEY_TITANIC_SUCCESS, data
    }
}
const doFilmKeyTitanicFailure = () => {
    return {
        type : FILM_KEY_TITANIC_FAILURE
    }
}


/** Here is the thunk action that is available to components */
export const doFilmKeyTitanic =  () => async (dispatch) => {
    // Fire start action to display a loading indicator for example
    dispatch(doFilmKeyTitanicStart());
    try {
      const data= await filmKeyTitanicApi();
      dispatch(doFilmKeyTitanicSuccess(data));
    } catch (error) {
       // on failure, fire failure action to e.g. show error message to user
      dispatch(doFilmKeyTitanicFailure(error.message));
    }
  };
  