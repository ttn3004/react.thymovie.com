import {
  FILM_ITEM_START,
  FILM_ITEM_SUCCESS,
  FILM_ITEM_FAILURE
} from '../constants/actionTypes';
import {
  FILM_ITEM_REV_START,
  FILM_ITEM_REV_FAILURE
} from '../constants/actionTypes';
import { filmItemApi } from '../api/film';
import { filmItemRevApi } from '../api/film';

const doFilmItemStart = () => {
  return {
    type: FILM_ITEM_START
  };
};
const doFilmItemSuccess = data => {
  return {
    type: FILM_ITEM_SUCCESS,
    data
  };
};
const doFilmItemFailure = error => {
  return {
    type: FILM_ITEM_FAILURE,
    error
  };
};

const doFilmItemRevStart = () => {
  return {
    type: FILM_ITEM_REV_START,
    error: ''
  };
};

const doFilmItemRevFailure = () => {
  return {
    type: FILM_ITEM_REV_FAILURE,
    error: ''
  };
};

export const doFilmItemRev = (rev, idFilm) => async dispatch => {
  dispatch(doFilmItemRevStart());
  try {
    const dataReview = await filmItemRevApi(rev, idFilm);
    const data = await filmItemApi(idFilm);
    dispatch(doFilmItemSuccess(data));
  } catch (error) {
    dispatch(doFilmItemRevFailure(error.message));
  }
};

//copy cai  nay truoc khi viet typeaction va de insérer api
/** Here is the thunk action that is available to components */
//phai dat id trong param doFilmItem de truyen id xuong cho filmItemApi
export const doFilmItem = id => async dispatch => {
  // Fire start action to display a loading indicator for example
  dispatch(doFilmItemStart());
  try {
    //phai dat id trong param filmItemApi(id) vi trong filmItemApi trong api co dat param la idItem
    const data = await filmItemApi(id);
    dispatch(doFilmItemSuccess(data));
  } catch (error) {
    // on failure, fire failure action to e.g. show error message to user
    dispatch(doFilmItemFailure(error.message));
  }
};
