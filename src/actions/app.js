import {
  APP_LOGIN_START,
  APP_LOGIN_SUCCESS,
  APP_LOGIN_FAILURE,
  APP_AUTH_SUCCESS,
  APP_BREADCRUMBS_SUCCESS,
  APP_SAVE_WISHLIST_START,
  APP_SAVE_WISHLIST_SUCCESS,
  APP_SAVE_WISHLIST_FAILURE,
  DELETE_WISHLIST_START,
  DELETE_WISHLIST_SUCCESS,
  DELETE_WISHLIST_FAILURE,
  DELETE_TO_MY_LIST_START,
  DELETE_TO_MY_LIST_SUCCESS,
  DELETE_TO_MY_LIST_FAILURE,
  ADD_TO_MY_LIST_START,
  ADD_TO_MY_LIST_SUCCESS,
  ADD_TO_MY_LIST_FAILURE
} from '../constants/actionTypes';
import { userAuthApi, addToMyListApi } from '../api/user';
import { loginUserApi, deleteToMyListApi } from '../api/user';
import {
  APP_LOGOUT_START,
  APP_LOGOUT_SUCCESS,
  APP_LOGOUT_FAILURE
} from '../constants/actionTypes';
import {
  logoutUserApi,
  userSaveWishListApi,
  deleteWishlistApi
} from '../api/user';

const doUserLoginStart = () => {
  return {
    type: APP_LOGIN_START,
    error: ''
  };
};
const doUserLoginSuccess = data => {
  return {
    type: APP_LOGIN_SUCCESS,
    error: '',
    data
  };
};
const doUserLoginFailure = error => {
  return {
    type: APP_LOGIN_FAILURE,
    error
  };
};

const doUserAuthSuccess = data => {
  return {
    type: APP_AUTH_SUCCESS,
    error: '',
    data
  };
};

const doUserLogoutStart = () => {
  return {
    type: APP_LOGOUT_START,
    error: ''
  };
};
const doUserLogoutSuccess = () => {
  return {
    type: APP_LOGOUT_SUCCESS,
    error: ''
  };
};
const doUserLogoutFailure = () => {
  return {
    type: APP_LOGOUT_FAILURE,
    error: ''
  };
};
const doBreadcrumbsSuccess = arr => {
  return {
    type: APP_BREADCRUMBS_SUCCESS,
    data: arr
  };
};

const doAppSaveWishListStart = () => {
  return {
    type: APP_SAVE_WISHLIST_START,
    error: ''
  };
};
const doAppSaveWishListSuccess = () => {
  return {
    type: APP_SAVE_WISHLIST_SUCCESS,
    error: ''
  };
};
const doAppSaveWishListFailure = error => {
  return {
    type: APP_SAVE_WISHLIST_FAILURE,
    error
  };
};

const doDeleteWishlistStart = () => {
  return {
    type: DELETE_WISHLIST_START,
    error: ''
  };
};
const doDeleteWishlistSuccess = () => {
  return {
    type: DELETE_WISHLIST_SUCCESS
  };
};
const doDeleteWishlistFailure = error => {
  return {
    type: DELETE_WISHLIST_FAILURE,
    error
  };
};

const doAddToMyListStart = () => {
  return {
    type: ADD_TO_MY_LIST_START,
    error: ''
  };
};
const doAddToMyListFailure = error => {
  return {
    type: ADD_TO_MY_LIST_FAILURE,
    error
  };
};

const doDeleteToMyListStart = () => {
  return {
    type: DELETE_TO_MY_LIST_START
  };
};
const doDeleteToMyListSuccess = () => {
  return {
    type: DELETE_TO_MY_LIST_SUCCESS
  };
};
const doDeleteToMyListFailure = error => {
  return {
    type: DELETE_TO_MY_LIST_FAILURE,
    error
  };
};

export const doBreadcrumbs = arr => dispatch => {
  dispatch(doBreadcrumbsSuccess(arr));
};

export const doSaveWishlist = name => async dispatch => {
  dispatch(doAppSaveWishListStart());
  try {
    const data = await userSaveWishListApi(name);
    dispatch(doAppSaveWishListSuccess());
    const dataUserApp = await userAuthApi();
    dispatch(doUserAuthSuccess(dataUserApp));
  } catch (error) {
    dispatch(doAppSaveWishListFailure(error.message));
  }
};

export const doDeleteWishlist = idWl => async dispatch => {
  dispatch(doDeleteWishlistStart());
  try {
    const data = await deleteWishlistApi(idWl);
    dispatch(doDeleteWishlistSuccess());
    const dataUserApp = await userAuthApi();
    dispatch(doUserAuthSuccess(dataUserApp));
  } catch (error) {
    dispatch(doDeleteWishlistFailure(error.message));
  }
};

export const doAddToMyList = (idFilm, idWl) => async dispatch => {
  dispatch(doAddToMyListStart());
  try {
    const dataWl = await addToMyListApi(idFilm, idWl);

    const dataUserApp = await userAuthApi();
    dispatch(doUserAuthSuccess(dataUserApp));
  } catch (error) {
    dispatch(doAddToMyListFailure(error.message));
  }
};

export const doDeleteToMyList = idWl_line => async dispatch => {
  dispatch(doDeleteToMyListStart());
  try {
    const data = await deleteToMyListApi(idWl_line);
    dispatch(doDeleteToMyListSuccess());
    const dataUserApp = await userAuthApi();
    dispatch(doUserAuthSuccess(dataUserApp));
  } catch (error) {
    dispatch(doDeleteToMyListFailure(error.message));
  }
};

export const doUserLogin = (email, pass) => async dispatch => {
  dispatch(doUserLoginStart());
  try {
    if (email.length <= 3 || pass.length < 3) {
      dispatch(doUserLoginFailure('Email ou mot de pass non valide'));
    } else {
      const token = await loginUserApi(email, pass);
      //getUserInfo() de lay infoUser tu tk
      const dataUser = await userAuthApi();
      if (dataUser.email === '') {
        dispatch(doUserLoginFailure('User not found'));
      }else{
        dispatch(doUserLoginSuccess(token));
        dispatch(doUserAuthSuccess(dataUser));
      }
      
    }
  } catch (error) {
    dispatch(doUserLoginFailure(error.message));
  }
};

//doUserAuth() dung cho App.js de lay thong tin user de hien thi ngay tu Home
export const doUserAuth = () => async dispatch => {
  dispatch(doUserLoginStart());
  try {
    //userAuthApi() de lay token trong localStorage
    const data = await userAuthApi();
    if (data.email === '') {
      dispatch(doUserLoginFailure('User not found'));
    }else{
      
      dispatch(doUserAuthSuccess(data));
    }
    
  } catch (error) {
    //dispatch(doUserLoginFailure(error.message));
  }
};

export const doUserLogout = () => dispatch => {
  dispatch(doUserLogoutStart());
  try {
    logoutUserApi();
    dispatch(doUserLogoutSuccess());
  } catch (error) {
    dispatch(doUserLogoutFailure(error.message));
  }
};
