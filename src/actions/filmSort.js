import { FILM_SORT_START, FILM_SORT_SUCCESS, FILM_SORT_FAILURE } from '../constants/actionTypes';
import {filmSortApi} from '../api/film';

const doFilmSortStart = () => {
    return {
        type : FILM_SORT_START
    }
}

const doFilmSortSuccess = (data) => {
    return {
        type : FILM_SORT_SUCCESS, data 
    }
}

const doFilmSortFailure = (error) => {
    return {
        type : FILM_SORT_FAILURE, error
    }
}

/** Here is the thunk action that is available to components */
export const doFilmSort =  () => async (dispatch) => {
    // Fire start action to display a loading indicator for example
    dispatch(doFilmSortStart());
    try {
      const data= await filmSortApi();
      dispatch(doFilmSortSuccess(data));
    } catch (error) {
       // on failure, fire failure action to e.g. show error message to user
      dispatch(doFilmSortFailure(error.message));
    }
    
    
  };